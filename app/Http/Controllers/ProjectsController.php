<?php

namespace App\Http\Controllers;

use App\Project;
use App\Task;
use Request;

class ProjectsController extends Controller
{
    protected $request;

    protected $rules = [
        'name' => 'required|min:3',
        'slug' => 'required',
    ];

    public function __construct(Request $request) {
        $this->request = $request;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $projects = Project::all();
        return view('projects.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return Response
     */
    public function store(\Illuminate\Http\Request $request)
    {
        $this->validate($request, $this->rules);

        $input = Request::all();
        Project::create( $input );

        return redirect('projects')->with('message', 'Project created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project $project
     * @return Response
     */
    public function show(Project $project)
    {
        return view('projects.show', compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project $project
     * @return Response
     */
    public function edit(Project $project)
    {
        return view('projects.edit', compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Project $project
     * @param \Illuminate\Http\Request $request
     * @return Response
     */
    public function update(Project $project, \Illuminate\Http\Request $request)
    {
        $this->validate($request, $this->rules);

        $input = array_except(Request::all(), '_method');
        $project->update($input);

        return redirect('projects/'.$project->slug)->with('message', 'Project updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project $project
     * @return Response
     */
    public function destroy(Project $project)
    {
        $project->delete();

        return redirect('projects')->with('message', 'Project deleted.');
}
}
