<?php

use Illuminate\Database\Seeder;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('projects')->delete();

        $projects = [];
        for($i = 0; $i < 10; $i++)
        {
            $projects[] =  [
                'name' => 'Project - '.$i, 'slug' => 'project-'.$i,
                    'created_at' => new DateTime, 'updated_at' => new DateTime
            ];
        }

        DB::table('projects')->insert($projects);
    }
}
