<?php

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->delete();

        $tasks = [];
        for($i = 0; $i < 10; $i++)
        {
            $tasks[] = [
                'name' => 'Task -'.$i, 'slug' => 'task-'.$i,
                    'project_id' => rand(1, 2),
                    'completed' => false,
                    'description' => str_random(50),
                    'created_at' => new DateTime, 'updated_at' => new DateTime
            ];
        }

        DB::table('tasks')->insert($tasks);
    }
}
