## Project Aim

> Our to-do application will consist of one or more projects, each with its own list of tasks. You will be able to create, list, modify and delete both tasks and projects. ([Creating a Basic ToDo Application in Laravel 5 – Part 1](https://www.flynsarmy.com/2015/02/creating-a-basic-todo-application-in-laravel-5-part-1/))

### Features

- Route and Model binding using [LaravelCollective](https://laravelcollective.com/)
- Complex ORM (parent has many children)